-- S03 - CRUD OPERATIONS


-- CREATE --

-- ADD A RECORD IN OUR artists TABLE
	-- Syntax: INSERT INTO table_name (column_name) VALUES (value1)
	INSERT INTO artists (name) VALUES ("Blackpink");
	INSERT INTO artists (name) VALUES ("Twice");
	INSERT INTO artists (name) VALUES ("Taylor Swift");

-- Display all columns of all artists
	-- Syntax: SELECT *(all) FROM table_name;
	SELECT * FROM artists;

-- Display a specific column of records
	-- Syntax: SELECT column_name FROM table_name;
	SELECT name FROM artists;

-- Add records/rows in albums TABLE
	-- Add a record with multiple columns
	-- Syntax: INSERT INTO table_name (column_name, column_name) VALUES (value1, value2)
	INSERT INTO albums (album_title, date_released, artist_id) VALUES ("The Album", "2020-10-02", 1);
	INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fancy You", "2019-04-22", 2);
	INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-10-02", 3);

-- Add multiple records in songs TABLE
	-- Syntax: INSERT INTO table_name (column_name, column_name) VALUES (value1, value2), (value1, value2)
	INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fancy", 333, "K-Pop", 2), ("How You Like That", 301, "K-Pop", 1), ("Lovesick Girls", 312, "K-Pop", 1), ("All too well(Taylor's version)", 1000, "Pop", 3);

-- Display/Retrieve records with selected columns
	-- Syntax: SELECT (column_name1, column_name2) FROM table_name;
	SELECT song_name, genre FROM songs;

-- Display/Retrieve records with certain conditions
	-- WHERE clauase/keyword
	-- Syntax: SELECT column_name FROM table_name WHERE condition;
	SELECT song_name FROM songs WHERE genre = "K-Pop";
	SELECT song_name FROM songs WHERE length > 301;

-- Retrieve records with multiple conditions using the AND CLAUSE and OR CLAUSE
-- AND
	SELECT song_name FROM songs WHERE genre = "Pop" AND length = 1000;
-- OR
	SELECT song_name FROM songs WHERE genre = "K-Pop" OR length < 200;


-- UPDATE --

-- Adding song for update example
	-- ex. Add a record to update:
	INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Paraluman", 410, "OPM", 2);

-- Update Records
	-- Syntax: UPDATE table_name SET column_name = value WHERE condition;
	UPDATE songs SET song_name = "Feel Special" WHERE song_name = "Paraluman";

-- Update multiple columns in our records
	-- Syntax: UPDATE table_name SET column_name = value, column_name2 = value WHERE condition;
	UPDATE songs SET length = 510, genre = "K-Pop" WHERE song_name = "Feel Special";


-- DELETE --

-- Deleting Records
	-- Syntax: DELETE FROM table_name WHERE condition;
	DELETE FROM songs WHERE genre = "Pop" AND length >= 1000;

-- Delete all table content
	-- Syntax: DELETE FROM table_name;


-- TLDR --
/*
	CREATE = INSERT
	READ = SELECT
	UPDATE = UPDATE
	DELETE = DELETE
*/